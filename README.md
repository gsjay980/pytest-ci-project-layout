# pytest CI project layout

[![pipeline status](https://gitlab.com/BCLegon/pytest-ci-project-layout/badges/master/pipeline.svg)](https://gitlab.com/BCLegon/pytest-ci-project-layout/commits/master)
[![coverage report](https://gitlab.com/BCLegon/pytest-ci-project-layout/badges/master/coverage.svg)](https://gitlab.com/BCLegon/pytest-ci-project-layout/commits/master)

A boilerplate for testing Python code with pytest using GitLab CI. Layout based on the [pytest documentation](https://docs.pytest.org/en/latest/goodpractices.html#tests-outside-application-code).